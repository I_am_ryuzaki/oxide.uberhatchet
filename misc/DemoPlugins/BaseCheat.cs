﻿using Network;
using Oxide.Core.Plugins;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("BaseCheat", "Uberhatchet", "0.0.6")]
    public class BaseCheat : RustPlugin
    {
        #region [Class] Settings
        public class Settings
        { 
            public class FriendSystem
            {
                public static bool FriendlyFireEnable { get; set; }
                public static List<ulong> FriendlyList { get; set; }
            }

            public class AimSystem
            {
                public static bool AimAssistEnable { get; set; }
                public static int AimAssistChanse { get; set; }
            }

            public class NoRecoilSystem
            {
                public static bool Enable { get; set; }
            }

            public class DaySystem
            {
                public static bool AllDayEnable { get; set; }
            }
        }
        #endregion

        #region [Method] LoadDefaultConfig()
        protected override void LoadDefaultConfig()
        {
            // Ниже формат конфига который будет записан по уомолчанию
            this.Config["FriendSystem"] = new Dictionary<string, object>()
            {
                { "FriendlyFireEnable", true },
                { "FriendlyList", "76561234567891234,76561234567891235" }
            };

            this.Config["AimSystem"] = new Dictionary<string, object>()
            {
                { "AimAssistEnable", true },
                { "AimAssistChanse", 70 }
            };

            this.Config["NoRecoilSystem"] = new Dictionary<string, object>()
            {
                { "Enable", true }
            };

            this.Config["DaySystem"] = new Dictionary<string, object>()
            {
                { "AllDayEnable", true }
            };
            // Сохраняем сформированный КОНФИГ
            this.Config.Save();
        }
        #endregion

        #region [Method] LoadSavedConfig()
        void LoadSavedConfig()
        {
            // Загружаем конфиг - всё что мы там на сейвили. 
            this.Config.Load();

            // Ниже мы считываем из конфигша которй мы прочитали из файла, в класс Settings - заполняем поля которые мы сами сделали под данные из конфига.
            Settings.FriendSystem.FriendlyFireEnable = (bool)((Dictionary<string, object>)this.Config["FriendSystem"])["FriendlyFireEnable"];
            Settings.FriendSystem.FriendlyList = new List<ulong>();
            string[] users = ((string)((Dictionary<string, object>)this.Config["FriendSystem"])["FriendlyList"]).Replace(" ", "").Split(new char[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < users.Length; ++i) 
            {
                try
                {
                    Settings.FriendSystem.FriendlyList.Add(ulong.Parse(users[i]));
                    this.Puts("FriendSystem.FriendlyList, loaded friend: " + users[i]);
                }
                catch (System.Exception ex)
                {
                    this.PrintError("Loading config, FriendSystem.FriendlyList[" + ulong.Parse(users[i]) + "]: continue " + ex.Message);
                }
            }
            Settings.AimSystem.AimAssistEnable = (bool)((Dictionary<string, object>)this.Config["AimSystem"])["AimAssistEnable"];
            Settings.AimSystem.AimAssistChanse = (int)((Dictionary<string, object>)this.Config["AimSystem"])["AimAssistChanse"];
            Settings.NoRecoilSystem.Enable = (bool)((Dictionary<string, object>)this.Config["NoRecoilSystem"])["Enable"];
            Settings.DaySystem.AllDayEnable = (bool)((Dictionary<string, object>)this.Config["DaySystem"])["AllDayEnable"];
        }
        #endregion

        #region [Method] OnClientDisconnected()
        void OnClientDisconnected(string reason)
        {
            // Взываем отключение ESPWorker
            ESPWorker.UnsetComponent();
        }
        #endregion

        #region [Method] Loaded()
        void Loaded()
        {
            // Вызываем функцию загрузки конфига
            this.LoadSavedConfig();
            // Вызываем подключение ESPWorker
            ESPWorker.SetComponent();
        }
        #endregion

        #region [Method] Unload()
        void Unload()
        {
            // Взываем отключение ESPWorker
            ESPWorker.UnsetComponent();
        }
        #endregion

        #region [Method] OnFinishLoading()
        void OnFinishLoading(BasePlayer player)
        {
            // Вызываем подключение ESPWorker
            ESPWorker.SetComponent();
        }
        #endregion

        #region[Method] OnConsoleCommandfromServer(Network.Message message)
        object OnConsoleCommandfromServer(Network.Message message)
        {
            // Читаем из пакета строку(текст)
            string line = message.read.String();
            // Если строка более или ровна 6 символам и начинается с строки(текста) niclip то вызываем return false
            if (line.Length >= 6 && line.StartsWith("noclip"))
            {
                 this.Puts("Server send console command: " + line);
                return false;
            }
            // Если мы дошли до этого места - значит сверху return false не выполнялся, значит возвращаем обратно позицию чтения пакетьа в начало.
            message.read.Position = 1;
            // вызываем return null что разрешит клиенту игры самому отработать эту команду.
            return null;
        }
        #endregion

        #region [Method] OnEntityCreate(BaseEntity entity, ProtoBuf.Entity protoEntity)
        void OnEntityCreate(BaseEntity entity, ProtoBuf.Entity protoEntity)
        {
            // Если обьект является игроком
            if (entity is BasePlayer)
            {
                // Для удобства работы конвертируем тип в игрока
                BasePlayer player = (BasePlayer)entity;
                // Проверяем, если SteamID у игрока равен нашему под которым работает клиент игры.
                if (player.userID == SteamClient.localSteamID)
                {
                    // Добавляем флаг админа в нашем клиенте игры.
                    player.playerFlags |= BasePlayer.PlayerFlags.IsAdmin;
                }
            }
            
            // Если обьект является оружием(Не ближнего боя)
            if (entity is BaseProjectile)
            {
                // Для удобства работы конвертируем тип в оружие
                BaseProjectile projectile = (BaseProjectile)entity;
                // Проверяем по конфигу, надо ли выключать отдачу у оружия
                if (Settings.NoRecoilSystem.Enable)
                {
                    // Проверяем - есть ли у данного оружия отдача
                    if (projectile.recoil != null)
                    {
                        // Выкручиваем все показатили отдачи на 0
                        projectile.recoil.recoilPitchMax = 0;
                        projectile.recoil.recoilPitchMin = 0;
                        projectile.recoil.recoilYawMax = 0;
                        projectile.recoil.recoilYawMin = 0;
                        projectile.recoil.timeToTakeMin = 0;
                    }
                }
                // Сокращаем время перезарядки оружия на 5% путем умножения времяни перезарядки на 0.95 - что вернет число равное 95% от того что было.
                projectile.reloadTime = projectile.reloadTime * 0.95f;
            }
        }
        #endregion

        #region [Method] OnEntityUpdate(BaseEntity entity, ProtoBuf.Entity protoEntity)
        void OnEntityUpdate(BaseEntity entity, ProtoBuf.Entity protoEntity)
        {
            // Если от сервера мы получили информацию о игроке и SteamID полученого игрока равен нашему под которым работает клиент игры
            if (protoEntity.basePlayer != null && protoEntity.basePlayer.userid == SteamClient.localSteamID)
            {
                // Конвертируем сумму флагов в флаги для удобной работы в дальнейшем
                BasePlayer.PlayerFlags playerFlags = (BasePlayer.PlayerFlags)protoEntity.basePlayer.playerFlags;
                // Добавляем флаг админа в список флагов.
                playerFlags |= BasePlayer.PlayerFlags.IsAdmin;
                // Конвертируем обратно флаги в число.
                protoEntity.basePlayer.playerFlags = (int)playerFlags;

                // Если уровень воды в получаемом пакете от сервера меньше 10
                if (protoEntity.basePlayer.metabolism.hydration < 10)
                {
                    // Устаналиваем в пакете уровень воды 10
                    protoEntity.basePlayer.metabolism.hydration = 10;
                }
            }
            // Проверяем в настройках - надо ли нам держать включенным вечный день
            if (Settings.DaySystem.AllDayEnable)
            {
                // Если от сервера мы получили информацию о системе погоды и времени суток
                if (protoEntity.environment != null)
                {
                    // Меняем время суток на день. ps - что за число? Не спрашивайте, сам хз. Просто подглядел когдато что это день и всё.
                    protoEntity.environment.dateTime = 5250206760382237147L;
                }
            }
        }
        #endregion

        #region [Method] OnSendClientTick(PlayerTick playerTick)
        void OnSendClientTick(PlayerTick playerTick)
        {
            // Если в пакете присутсвует информиация о том, что игрок летает(состояние модели игрока в полете)
            if (playerTick.modelState.flying == true)
            {
                // То меняем значение в отправляемом пакете - на отсутсвие полета(Состояние модели игрока в полете)
                playerTick.modelState.flying = false;
            }
        }
        #endregion

        #region [Method] OnProjectileAttack(ProtoBuf.PlayerProjectileAttack projectile)
        object OnProjectileAttack(ProtoBuf.PlayerProjectileAttack projectile)
        {
            // Если мы паопали в что либо а не в декорации или землю
            if (projectile.playerAttack.attack.hitID != 0)
            {
                // Находим обьект в который мы попали
                BaseEntity baseEntity = global::BaseNetworkable.clientEntities.Find(projectile.playerAttack.attack.hitID) as BaseEntity;
                // Если обьект есть и он является игроком
                if (baseEntity != null && baseEntity is BasePlayer)
                {
                    // Конвертируем тип в BasePlayer
                    BasePlayer player = (BasePlayer)baseEntity;
                    // Проверяем - если SteamID игрока есть в списке друзей
                    if (Settings.FriendSystem.FriendlyList.Contains(player.userID))
                    {
                        // Блокируем отпрвавку данных
                        return false;
                    }
                    // Включено ли в настройках попадание в голову?
                    if (Settings.AimSystem.AimAssistEnable)
                    {
                        // Вставляем в функцию проверки шанса, тот шанс который в настройках
                        if (this.GetChanseResult(Settings.AimSystem.AimAssistChanse))
                        {
                            // Изменяем кость на голову.
                            projectile.playerAttack.attack.hitBone = 698017942;
                            projectile.playerAttack.attack.hitPartID = 2173623152;
                            projectile.playerAttack.attack.hitPositionLocal = new UnityEngine.Vector3(-0.1f, -0.1f, 0.0f);
                            projectile.playerAttack.attack.hitNormalLocal = new UnityEngine.Vector3(0.0f, -1.0f, 0.0f);
                        }
                    }
                }
            }
            return null;
        }
        #endregion

        #region [Method] OnMeleeAttack(ProtoBuf.PlayerAttack playerAttack)
        object OnMeleeAttack(ProtoBuf.PlayerAttack playerAttack)
        {
            // Если мы паопали в что либо а не в декорации или землю
            if (playerAttack.attack.hitID != 0)
            {
                // Находим обьект в который мы попали
                BaseEntity baseEntity = global::BaseNetworkable.clientEntities.Find(playerAttack.attack.hitID) as BaseEntity;
                // Если обьект есть и он является игроком
                if (baseEntity != null && baseEntity is BasePlayer)
                {
                    // Конвертируем тип в BasePlayer
                    BasePlayer player = (BasePlayer)baseEntity;
                    // Проверяем - если SteamID игрока есть в списке друзей
                    if (Settings.FriendSystem.FriendlyList.Contains(player.userID))
                    {
                        // Блокируем отпрвавку данных
                        return false;
                    }
                    // Включено ли в настройках попадание в голову?
                    if (Settings.AimSystem.AimAssistEnable)
                    {
                        // Вставляем в функцию проверки шанса, тот шанс который в настройках
                        if (this.GetChanseResult(Settings.AimSystem.AimAssistChanse))
                        {
                            // Изменяем кость на голову.
                            playerAttack.attack.hitBone = 698017942;
                            playerAttack.attack.hitPartID = 2173623152;
                            playerAttack.attack.hitPositionLocal = new UnityEngine.Vector3(-0.1f, -0.1f, 0.0f);
                            playerAttack.attack.hitNormalLocal = new UnityEngine.Vector3(0.0f, -1.0f, 0.0f);
                        }
                    }
                }
            }
            return null;
        }
        #endregion

        #region [Method] OnPlayerLanded(float velocity)
        object OnPlayerLanded(float velocity)
        {
            // Вовращаем при подении значение 0 не зависимо не от чего.
            return 0f;
        }
        #endregion

        #region [Method] GetChanseResult(int procent)
        public bool GetChanseResult(int procent)
        {
            int random = UnityEngine.Random.Range(0, 100);
            return random <= procent;
        }
        #endregion

        #region [Class] ESPWorker
        public class ESPWorker : UnityEngine.MonoBehaviour
        {
            private float m_tickerUpdateFixed = 0f;

            public static void UnsetComponent()
            {
                // Если существует активный Terrain
                if (UnityEngine.Terrain.activeTerrain != null)
                {
                    // Получаем все компоненты с активного Terrain
                    MonoBehaviour[] components = UnityEngine.Terrain.activeTerrain.gameObject.GetComponents<MonoBehaviour>();
                    // Проходим по каждому компоненту
                    for (int i = 0; i < components.Length; ++i)
                    {
                        // Если название класса(типа) является ESPWorker
                        if (components[i].GetType().Name == "ESPWorker")
                        {
                            // Уничтожаем компонент
                            UnityEngine.Component.Destroy(components[i]);
                        }
                    }
                }
            }

            public static void SetComponent()
            {
                // На всякий случай пытаемся уничтожить компоненты если есть.
                ESPWorker.UnsetComponent();
                // Если существует активный Terrain и существует локальный игрок
                if (UnityEngine.Terrain.activeTerrain != null && LocalPlayer.Entity != null)
                {
                    // Добавляем новый компонент ESPWorker к акнтивному Terrain
                    UnityEngine.Terrain.activeTerrain.gameObject.AddComponent<ESPWorker>();
                }
            }
            
            void Update()
            {
                // Локальный игрок должен быть инициализирован, и мы проверяем - не null ли часом он.
                if (LocalPlayer.Entity != null)
                {
                    this.m_tickerUpdateFixed += UnityEngine.Time.deltaTime;
                    if (this.m_tickerUpdateFixed >= 0.3f)
                    {
                        this.m_tickerUpdateFixed = 0f;
                        // Берем список игроков которые нам отправил сервер и проходися по каждому
                        for (int i = 0; i < BasePlayer.VisiblePlayerList.Count; ++i)
                        {
                            // Если игроку по которому мы проходим не является локальным игроком, то действуем дальше
                            if (BasePlayer.VisiblePlayerList[i] != LocalPlayer.Entity)
                            {
                                // Получаем и записываем дистанцию от этого ESP игрока до нашего локального игрока.
                                int distance = (int)UnityEngine.Vector3.Distance(BasePlayer.VisiblePlayerList[i].transform.position, LocalPlayer.Entity.transform.position);
                                // Проверяем что бы дистанция была не более 300 метров, иначе эти игроки видимо зависшие фантомы в клиенте так как сервер уже не отправляет их вам.
                                if (distance <= 300)
                                {
                                    // Отрисовываем ESP на игрока.
                                    UnityEngine.DDraw.Text($"<size=12>{BasePlayer.VisiblePlayerList[i].displayName} [{(int)BasePlayer.VisiblePlayerList[i].health} hp] [{distance} m]\n<color=orange>{BasePlayer.VisiblePlayerList[i].GetHeldEntity()?.GetItem().info.shortname}</color></size>",
                                        BasePlayer.VisiblePlayerList[i].transform.position, (BasePlayer.VisiblePlayerList[i].HasPlayerFlag(BasePlayer.PlayerFlags.Sleeping) ? UnityEngine.Color.red : UnityEngine.Color.green), 0.3f);
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
}