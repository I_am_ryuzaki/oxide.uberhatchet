﻿using Network;
using Oxide.Core.Plugins;
using System.Linq;

namespace Oxide.Plugins
{
    [Info("DemoHooks", "Uberhatchet", "0.0.4")]
    public class DemoHooks : RustPlugin
    {
        /// <summary>
        /// Срабатывает в самом начале когда раст только еще запускается и появляется консоль. Или перед Loaded после перезапуска плагина
        /// </summary>
        void Init()
        {
            Puts("Init");
        }

        /// <summary>
        /// Срабатывает когда этот плагин официально загрузился(включился)
        /// </summary>
        void Loaded()
        {
            Puts("Loaded");
        }

        /// <summary>
        /// Срабатывает когда конфиг плагина отсутсвует и система пытаетя получить стандартные значения.
        /// </summary>
        protected override void LoadDefaultConfig()
        {
            Puts("LoadDefaultConfig");
        }


        /// <summary>
        /// Срабатывает когда сторонний плагин официально загрузился(включился)
        /// </summary>
        /// <param name="name">Экземпляр плагина</param>
        void OnPluginLoaded(Plugin name)
        {
            Puts("OnPluginLoaded");
        }

        /// <summary>
        /// Срабатывает когда сторонний плагин официально выгрузился(выключился)
        /// </summary>
        /// <param name="name">Экземпляр плагина</param>
        void OnPluginUnloaded(Plugin name)
        {
            Puts("OnPluginUnloaded");
        }


        /// <summary>
        /// Срабатывает когда плагин выгружается(отключается)
        /// </summary>
        void Unload()
        {
            Puts("Unload");
        }

        /// <summary>
        /// Срабатывает когда завершается загрузка и вы попадаете в главное меню
        /// </summary>
        void OnRustInitialized()
        {
            Puts("OnRustInitialized");
        }

        /// <summary>
        /// Срабатывает когда игрока кикает.
        /// Не обязательно с сервера, коннект может не состоятся а его кикнет в главное меню.
        /// </summary>
        /// <param name="reason">Причина кика</param>
        void OnClientDisconnected(string reason)
        {
            Puts("OnClientDisconnected: " + reason);
        }

        /// <summary>
        /// Срабатывает перед тем как экрану загрузки пропасть.
        /// </summary>
        /// <param name="player">Экземпляр локального игрока от имени которого был вызов хука.</param>
        void OnFinishLoading(BasePlayer player)
        {
            // К примепру, в конце загрузки нужно найти друга что бы понять, в онлайне он или нет - если он рядом конечно.
            Puts("OnFinishLoading");
        }

        /// <summary>
        /// Срабатывает когда сервер принял ваше подключение и запросил ваши данные, здесь можно подсунуть свои данные о себе.
        /// </summary>
        /// <returns>Если вернуть что угодно - не null, то код не выполнил действия в Rust по отправки данные, а оставит это дело вам!</returns>
        object OnRequestUserInformation()
        {
            Puts("OnRequestUserInformation");
            return null;
        }
        
        /// <summary>
        /// Срабатывает когда сервер только только начинает вам отправлять данные об окружающем мире.
        /// Обычно происходит после завершения загрузки карты при подключении, или когда вы возраждаетесь или телепортируетесь и у вас появляется экран загрузки.
        /// Кароч это старт Receiving Data
        /// </summary>
        /// <param name="player"></param>
        void OnStartLoading(BasePlayer player)
        {
            Puts("OnStartLoading");
        }

        /// <summary>
        /// Срабатывает когда появляется новый обьект в игре.
        /// Внимание: Срабатывает после того как обьект полностью создан в игре.
        /// </summary>
        /// <param name="entity">Экземпляр обьекта в Rust - уже созданный</param>
        /// <param name="protoEntity">Экземпляр обьекта который передавал сервер(Лишь для того что бы знать что нам сервер присылал)</param>
        void OnEntityCreate(BaseEntity entity, ProtoBuf.Entity protoEntity)
        {
            //Puts("OnEntityCreate");
        }

        /// <summary>
        /// Срабатывает когда сервер присылает обновленную информацию об обьекте.
        /// Внимание: Срабатывает перед тем как обновит информацию об обьекте в самом клиенте!
        /// </summary>
        /// <param name="entity">Экземпляр обьекта в Rust</param>
        /// <param name="protoEntity">Экземпляр обьекта который передавал сервер(Тут можно и нужно менять инфу тут)</param>
        void OnEntityUpdate(BaseEntity entity, ProtoBuf.Entity protoEntity)
        {
            //Puts("OnEntityUpdate");
        }

        /// <summary>
        /// Срабатывает когда обьект уничтожается.
        /// </summary>
        /// <param name="entity">Экземпляр обьекта в игре</param>
        void OnEntityDestroy(BaseEntity entity)
        {
            //Puts("OnEntityDestroy");
        }

        /// <summary>
        /// Сроабатывает когда сервер присылает консольную команду на игру и заставляет выполнить её
        /// </summary>
        /// <param name="message">Пакет пришедший с сервера</param>
        /// <returns>Если вернуть что угодно - не null, то код не выполнил действия в Rust обработке команды, а оставит это дело вам!</returns>
        object OnConsoleCommandfromServer(Network.Message message)
        {
            Puts("OnConsoleCommandfromServer");
            return null;
        }

        /// <summary>
        /// Срабатывает когда сервер перемещает игрока при телепортации или просто отбрасывает назад не давая пройти.
        /// </summary>
        /// <param name="player">Экземпляр игрока которого перемещает(Локальный игрок)</param>
        /// <param name="pos">Позиция куда перемещает</param>
        void OnPlayerForcePositionTo(BasePlayer player, UnityEngine.Vector3 pos)
        {
            Puts("OnPlayerForcePositionTo");
        }

        /// <summary>
        /// Отрабатывается когда клиент отправляет на сервер информацию о текущем положении, состоянии и движении игрока.
        /// Внимание: Срабатывает после сбора информации в пакет но прям перед тем как отправится на сервер, так что можно менять инфу - что получит сервер.
        /// </summary>
        /// <param name="playerTick">Отправляемый экземпляр пакета информации о BasePlayer</param>
        void OnSendClientTick(PlayerTick playerTick)
        {
            //Puts("OnSendClientTick");
        }

        /// <summary>
        /// Сроабатывает когда сервер присылает удаленный вызов функции для какого либо обьекта в игре.
        /// </summary>
        /// <param name="entity">Обьект у которого должна быть вызвана функция</param>
        /// <param name="methodUID">Номер функции в StringPool, получить названием можно при помощи: StringPool.Get(methodUID)</param>
        /// <param name="initiatorSteamID">SteamID игрока если это действие ретранслируется от действий какого либо игрока. Например стрельба.</param>
        /// <param name="messag">Экземпляр пакета с оставшимися данными для аргументов и данными</param>
        /// <returns>Если вернуть что угодно - не null, то код не выполнил удаленно вызвонную функцию, а оставит это дело вам!</returns>
        object OnRPCMessage(BaseEntity entity, uint methodUID, ulong initiatorSteamID, Network.Message messag)
        {
            //Puts("OnRPCMessage");
            return null;
        }

        /// <summary>
        /// Сроабатывает когда вы попали во что либо из оружия дальнего боя(Не метательное)
        /// Внимание: Срабатывает ровно перед тем как отправить данные о попадании на сервер.
        /// </summary>
        /// <param name="projectile">Экземпляр данных отправляемых на сервер о попадании</param>
        /// <returns>Если вернуть что угодно - не null, то код не выполнил отправку на сервер, а оставит это дело вам!</returns>
        object OnProjectileAttack(ProtoBuf.PlayerProjectileAttack projectile)
        {
            //Puts("OnProjectileAttack");
            return null;
        }

        /// <summary>
        /// Срабатывает когда вы попали во что либо из оружия ближнего боя.(Не метательное)
        /// Внимание: Срабатывает ровно перед тем как отправить данные о попадании на сервер.
        /// </summary>
        /// <param name="playerAttack">Экземпляр данных отправляемых на сервер о попадании</param>
        /// <returns>Если вернуть что угодно - не null, то код не выполнил отправку на сервер, а оставит это дело вам!</returns>
        object OnMeleeAttack(ProtoBuf.PlayerAttack playerAttack)
        {
            //Puts("OnMeleeAttack");
            return null;
        }

        /// <summary>
        /// Срабатывает когда текущий игрок падает откуда либо на плоскость
        /// Внимание: Срабатывает прям перед отправкой на сервер.
        /// </summary>
        /// <param name="velocity">Уроень падения(тут своя какаято велечена)</param>
        /// <returns>Если вернуть число типа float, то это число заменит то - которое изначально пришло в отправке на сервер!</returns>
        object OnPlayerLanded(float velocity)
        {
            Puts("OnPlayerLanded");
            return null;
        }

        /// <summary>
        /// Срабатывает когда какой либо игрок атакован(точно пока хз, мб даже просто получил урон)
        /// </summary>
        /// <param name="attackedPlayer">Экземпляр игрока который был атакован</param>
        /// <param name="hitInfo">Информация о попадании по игроку</param>
        void OnPlayerAttacked(BasePlayer attackedPlayer, HitInfo hitInfo)
        {
            Puts("OnPlayerAttacked");
        }

        /// <summary>
        /// Срабатывает когда какой либо игрок умирает
        /// </summary>
        /// <param name="player">Экземпляр умершего игрока</param>
        void OnPlayerDied(BasePlayer player)
        {
            Puts("OnPlayerDied");
        }

        /// <summary>
        /// Срабатываект локальный игрок начинает лечится
        /// Внимание: Срабатывает прям перед тем как информация будет отправлена на сервер.
        /// </summary>
        /// <param name="medicalTool">Экземпляр медецинского прибора который используется</param>
        void OnMedicalUsing(MedicalTool medicalTool)
        {
            Puts("OnMedicalUsing");
        }
    }
}