﻿using Oxide.Core;
using Oxide.Core.Plugins;
using System;
using System.Reflection;
using Oxide.Rust;
using UnityEngine;

namespace Oxide.Plugins
{
    public abstract class RustPlugin : CSharpPlugin
    {

        public override void HandleAddedToManager(PluginManager manager)
        {
            foreach (FieldInfo field in GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                object[] attributes = field.GetCustomAttributes(typeof(OnlinePlayersAttribute), true);
                if (attributes.Length > 0)
                {
                    PluginFieldInfo pluginField = new PluginFieldInfo(this, field);
                    if (pluginField.GenericArguments.Length != 2 || pluginField.GenericArguments[0] != typeof(BasePlayer))
                    {
                        Puts($"The {field.Name} field is not a Hash with a BasePlayer key! (online players will not be tracked)");
                        continue;
                    }

                    if (!pluginField.LookupMethod("Add", pluginField.GenericArguments))
                    {
                        Puts($"The {field.Name} field does not support adding BasePlayer keys! (online players will not be tracked)");
                        continue;
                    }

                    if (!pluginField.LookupMethod("Remove", typeof(BasePlayer)))
                    {
                        Puts($"The {field.Name} field does not support removing BasePlayer keys! (online players will not be tracked)");
                        continue;
                    }

                    if (pluginField.GenericArguments[1].GetField("Player") == null)
                    {
                        Puts($"The {pluginField.GenericArguments[1].Name} class does not have a public Player field! (online players will not be tracked)");
                        continue;
                    }

                    if (!pluginField.HasValidConstructor(typeof(BasePlayer)))
                    {
                        Puts($"The {field.Name} field is using a class which contains no valid constructor (online players will not be tracked)");
                        continue;
                    }

                    onlinePlayerFields.Add(pluginField);
                }
            }

            base.HandleAddedToManager(manager);
        }

        public bool AddUI(string json)
        {
            CommunityEntity communityEntity = global::CommunityEntity.ClientInstance;
            if (communityEntity != null)
            {
                global::BaseEntity.RPCMessage msg = new BaseEntity.RPCMessage {read = NetworkReaderEx.GetReaderFromString(json)};
                communityEntity.AddUI(msg);
                return true;
            }

            return false;
        }

        public bool DestroyUI(string json)
        {
            CommunityEntity communityEntity = global::CommunityEntity.ClientInstance;
            if (communityEntity != null)
            {
                global::BaseEntity.RPCMessage msg = new BaseEntity.RPCMessage {read = NetworkReaderEx.GetReaderFromString(json)};
                communityEntity.DestroyUI(msg);
                return true;
            }
            return false;
        }
    }
}
