﻿using System;
using System.Collections.Generic;
using System.IO;
using ILRepacking;

namespace MergeBuild
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("MergeBuild from Oxide.Uberhatchet has been started!");
                string pathRoot = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.Parent.Parent.FullName;
                Console.WriteLine("SET DirectoryRoot: " + pathRoot);
                string pathBuilded = pathRoot + "/src/Oxide.Rust/src/bin/Bundle/Oxide.Rust/RustDedicated_Data/Managed/";
                Console.WriteLine("SET DirectoryBuilded: " + pathBuilded);
                if (Directory.Exists(pathBuilded) == false)
                    Error("Directory not found: " + pathBuilded);
                Console.WriteLine("Starting merge...");
                ILRepack ilRepack = new ILRepack(new RepackOptions
                {
                    InputAssemblies = new string[]
                    {
                        pathBuilded + "/Oxide.Core.dll",
                        pathBuilded + "/Oxide.CSharp.dll",
                        pathBuilded + "/Oxide.References.dll",
                        pathBuilded + "/Oxide.Rust.dll",
                        pathBuilded + "/Oxide.Unity.dll",
                    },
                    OutputFile = pathBuilded + "/Oxide.Core.dll",
                    SearchDirectories = new List<string> {pathBuilded},
                    AllowDuplicateResources = true,
                    TargetKind = ILRepack.Kind.Dll,
                    TargetPlatformVersion = "v4",
                });
                ilRepack.Repack();
                Console.WriteLine("Starting finish");
                //File.Delete(pathBuilded + "/Oxide.Core.dll");
                File.Delete(pathBuilded + "/Oxide.CSharp.dll");
                File.Delete(pathBuilded + "/Oxide.References.dll");
                File.Delete(pathBuilded + "/Oxide.Rust.dll");
                File.Delete(pathBuilded + "/Oxide.Unity.dll");
                //File.Move(pathBuilded + "/Oxide.Uberhatchet.dll", pathBuilded + "/Oxide.Core.dll");
                Console.WriteLine("Full finish");
            }
            catch (Exception ex)
            {
                Error("Fatal error: " + ex.Message);
            }
        }

        public static void Error(string message)
        {
            Console.WriteLine(message);
            Console.ReadLine();
            Environment.Exit(0);
        }
    }
}
